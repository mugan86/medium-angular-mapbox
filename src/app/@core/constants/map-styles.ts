export const STYLES = {
    STREETS: 'streets-v11',
    LIGHT: 'light-v10',
    DARK: 'dark-v10',
    OUTDOORS: 'outdoors-v11',
    SATELLITE: 'satellite-v9'
};
