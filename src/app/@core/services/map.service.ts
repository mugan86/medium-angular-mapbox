import { Injectable } from '@angular/core';
import { environment } from '@env/environment';
import { STYLES } from './../constants/map-styles';
import * as mapboxgl from 'mapbox-gl';
import * as MapboxGeocoder from '@mapbox/mapbox-gl-geocoder';
@Injectable({
  providedIn: 'root'
})
export class MapService {
  mapbox;
  map: mapboxgl.Map;
  style = `mapbox://styles/mapbox/${ STYLES.STREETS }`;
  lat = 43.1746;
  lng = -2.4125;
  zoom = 15;
  buildMap(geocoder: boolean = false) {
    this.mapbox = (mapboxgl as typeof mapboxgl);
    this.mapbox.accessToken = environment.mapBoxToken;
    // this.m
    this.map = new mapboxgl.Map({
        container: 'map',
        style: this.style,
        zoom: this.zoom,
        center: [this.lng, this.lat]
    });
    if (!geocoder) {
      this.map.addControl(new mapboxgl.NavigationControl());
    } else {
      this.map.addControl(new MapboxGeocoder({
        accessToken: environment.mapBoxToken,
        mapboxgl
      }));
    }
  }
}
