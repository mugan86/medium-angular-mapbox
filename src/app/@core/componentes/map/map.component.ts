import { Component, OnInit, Input } from '@angular/core';
import { MapService } from '@core/services/map.service';
import { DomSanitizer } from '@angular/platform-browser';
@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.css']
})
export class MapComponent implements OnInit {
  @Input() geocoder: boolean;
  constructor(private map: MapService, public sanitizer: DomSanitizer) { }

  ngOnInit() {
    console.log(this.geocoder)
    this.map.buildMap(this.geocoder);
  }

}
